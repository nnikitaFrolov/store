package com.nnikitafrolov.pmsystem.view;

import com.nnikitafrolov.pmsystem.model.Product;
import com.nnikitafrolov.pmsystem.services.ProductServices;
import com.nnikitafrolov.pmsystem.util.HibernateUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public class ProductView {

    ProductServices productServices;

    public ProductView() {
        this.productServices = new ProductServices();
    }

    private static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));


    public void view() {
        writeMessage("Input number page and count product in format: '1{2}'\n");

        try {
            String input = readString();
            String[] strings = input.split("\\{");

            int numberPage = Integer.valueOf(strings[0]);
            int countProduct = Integer.valueOf(strings[1].split("}")[0]);

            List<Product> products = productServices.getProductFromPage(numberPage, countProduct);
            for (Product product :
                    products) {
                writeMessage(product.toString() + "\n");
            }

            HibernateUtil.closeSessionFactory();
        } catch (IOException e) {
            e.printStackTrace();
        }

        view();
    }

    void writeMessage(String message) {
        System.out.println(message);
    }

    String readString() throws IOException {
        return bufferedReader.readLine();
    }
}
