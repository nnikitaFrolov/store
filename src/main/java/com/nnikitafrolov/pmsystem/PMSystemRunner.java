package com.nnikitafrolov.pmsystem;

import com.nnikitafrolov.pmsystem.view.ProductView;

public class PMSystemRunner {
    public static void main(String[] args) {
        ProductView view = new ProductView();
        view.view();
    }
}
