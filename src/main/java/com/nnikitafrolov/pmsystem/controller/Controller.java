package com.nnikitafrolov.pmsystem.controller;

public interface Controller<T, PK> {
    T getById(PK key);

    void save(T entity);

    void update(T entity);

    void remove(T entity);
}
