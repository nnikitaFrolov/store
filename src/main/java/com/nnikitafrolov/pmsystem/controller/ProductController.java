package com.nnikitafrolov.pmsystem.controller;

import com.nnikitafrolov.pmsystem.dao.hibernate.HibernateProductDAOImpl;
import com.nnikitafrolov.pmsystem.model.Product;

import java.util.List;

public class ProductController implements Controller<Product, Long> {
    private HibernateProductDAOImpl productDAO;

    public ProductController() {
        this.productDAO = new HibernateProductDAOImpl();
    }

    public Product getById(Long key) {
        return productDAO.getByPK(key);
    }

    public void save(Product entity) {
        productDAO.save(entity);
    }

    public void update(Product entity) {
        productDAO.update(entity);
    }

    public void remove(Product entity) {

        productDAO.delete(entity);
    }

    public List<Product> getMaxResultsFromFirstResult(int firstResults, int maxResult) {
        return productDAO.getMaxResultsFromFirstResult(firstResults, maxResult);
    }
}
