package com.nnikitafrolov.pmsystem.model;

public interface Identified<PK> {
    PK getId();
}
