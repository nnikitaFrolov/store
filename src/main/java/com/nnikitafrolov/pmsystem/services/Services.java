package com.nnikitafrolov.pmsystem.services;

public interface Services<T, PK> {
    T getById(PK key);

    void save(T entity);

    void update(T entity);

    void remove(T entity);
}
