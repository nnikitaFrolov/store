package com.nnikitafrolov.pmsystem.services;

import com.nnikitafrolov.pmsystem.controller.ProductController;
import com.nnikitafrolov.pmsystem.model.Product;

import java.util.List;

public class ProductServices implements Services<Product, Long> {

    private ProductController productController;
    private static final int SIZE_PAGE = 3;

    public ProductServices() {
        this.productController = new ProductController();
    }

    public Product getById(Long key) {
        return productController.getById(key);
    }

    public void save(Product entity) {
        productController.save(entity);
    }

    public void update(Product entity) {
        productController.update(entity);
    }

    public void remove(Product entity) {
        productController.remove(entity);
    }

    public List<Product> getProductFromPage(int numberPage, int countProduct) {
        return productController.getMaxResultsFromFirstResult(numberPage * SIZE_PAGE, countProduct);
    }
}
