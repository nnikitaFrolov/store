package com.nnikitafrolov.pmsystem.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtil {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        sessionFactory = new Configuration().configure("hibernate/hibernate.cfg.xml").buildSessionFactory();
        return sessionFactory;
    }

    public static void closeSessionFactory(){
        sessionFactory.close();
    }
}
