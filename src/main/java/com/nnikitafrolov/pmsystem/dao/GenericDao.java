package com.nnikitafrolov.pmsystem.dao;

import com.nnikitafrolov.pmsystem.model.Identified;

import java.util.List;

public interface GenericDao<T extends Identified<PK>, PK> {

    void save(T entity);

    T getByPK(PK key);

    void update(T entity);

    void delete(T entity);
}
