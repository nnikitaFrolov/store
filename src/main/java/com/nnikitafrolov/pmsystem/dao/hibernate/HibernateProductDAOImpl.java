package com.nnikitafrolov.pmsystem.dao.hibernate;

import com.nnikitafrolov.pmsystem.dao.GenericDao;
import com.nnikitafrolov.pmsystem.model.Product;
import com.nnikitafrolov.pmsystem.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

public class HibernateProductDAOImpl implements GenericDao<Product, Long> {
    public static final String ALL_PRODUCT = "FROM Product";
    private SessionFactory sessionFactory;

    public HibernateProductDAOImpl() {
        this.sessionFactory = HibernateUtil.getSessionFactory();
    }

    public void save(Product entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.save(entity);
        transaction.commit();
        session.close();
    }

    public Product getByPK(Long key) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        Product entity = session.get(Product.class, key);
        transaction.commit();
        session.close();
        return entity;
    }

    public void update(Product entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.merge(entity);
        transaction.commit();
        session.close();
    }

    public void delete(Product entity) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        session.delete(entity);
        transaction.commit();
        session.close();
    }

    public List<Product> getMaxResultsFromFirstResult(int firstResults, int maxResult) {
        Session session = sessionFactory.openSession();
        Transaction transaction;

        transaction = session.beginTransaction();
        Query<Product> query = session.createQuery(ALL_PRODUCT, Product.class);
        query.setFirstResult(firstResults);
        query.setMaxResults(maxResult);
        List<Product> products = query.list();
        transaction.commit();
        session.close();
        return products;
    }
}
